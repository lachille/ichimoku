#Ichimoku Kinkō Hyō usually shortened to "Ichimoku", 
# is a technical analysis method that builds on candlestick charting 
# to improve the accuracy of forecast price moves

from binance.client import Client
import os
import traceback
import time
import threading
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import json

api_key = os.environ['BINANCE_KEY']
api_secret = os.environ['BINANCE_SECRET_KEY']
client = Client(api_key, api_secret)


G_TS = {
			'1m' : 60000,
			'3m' : 60000 * 3,
			'5m' : 60000 * 5,
			'15m' : 60000 * 15,
			'30m' : 60000 * 30,
			'1h' : 3600000,
			'2h' : 3600000 * 2,
			'4h' : 3600000 * 4,
			'6h' : 3600000 * 6,
			'8h' : 3600000 * 8,
			'12h' : 3600000 * 12,
			'1d' : 86400000,
			'3d' : 86400000 * 3,
			'1w' : 604800000, 
			'1M' : 604800000 * 4
			}

#   [
#     1499040000000,      // Open time
#     "0.01634790",       // Open
#     "0.80000000",       // High
#     "0.01575800",       // Low
#     "0.01577100",       // Close
#     "148976.11427815",  // Volume
#     1499644799999,      // Close time
#     "2434.19055334",    // Quote asset volume
#     308,                // Number of trades
#     "1756.87402397",    // Taker buy base asset volume
#     "28.46694368",      // Taker buy quote asset volume
#     "17928899.62484339" // Ignore.
#   ]
class ichimoku:

	BASE_P = 9
	BASE_DISP = 9

	def __init__(self, timeScale, klineHist, interval, symbol, limit=1500, refreshRate=5, silent=False):
		self.name = "ichimoku"
		self.res = {'symbol': symbol, 'Interval': timeScale, 'tendancy':"",  "PK" : 0, "TK" : 0, "TKP" : 0, "TKK" : 0, "CP" : 0}
		self.klineHist =  klineHist
		self.symbol = symbol
		self.autoUpdate = False
		self.interval = interval
		self.limit = limit
		if self.klineHist is None:
			self.autoUpdate = True
		self.auto_update()
		self.timescaleTitle = timeScale
		self.timeScale = G_TS[timeScale]
		self.refreshRate = refreshRate
		self.silent = silent
		self.closetime = self.klineHist[0][6]
		self.closePrice = float(self.klineHist[0][4])
		self.last_price = self.closePrice

	def printer(self, str):
		if self.silent is False:
			print(str)

	def run(self):
		self.TENKAN_P = self.BASE_P
		self.KIJUN_P = self.BASE_P + 17
		self.CHIKOU_P = self.BASE_DISP + 17
		self.SS_DISP = self.BASE_DISP + 17
		self.SSB_P = self.KIJUN_P * 2
		self.auto_update()
		self.klineHist = self.klineHist[::-1]
		self.low = self.klineHist[0][3]
		self.high = self.klineHist[0][2]
		self.closetime = self.klineHist[0][6]
		self.closePrice = float(self.klineHist[0][4])
		self.ts = self.tenkan_sen(self.closetime)
		self.printer("tenkan : " + str(self.ts))
		self.ks = self.kijun_sen(self.closetime)
		self.printer("kijun : " + str(self.ks))
		self.printer("TK percent : " + str(self.TK_percent(self.closetime)))
		self.printer("TK  PRICE percent : " + str(self.TKPrice_percent(self.closetime, self.closePrice)))
		self.cs = self.chikou_span_percent(self.closetime)
		self.printer("chikou percent: " + str(self.cs))
		self.ssa, self.ssb = self.senkou_span(self.closetime)
		self.printer("ssa : " + str(self.ssa))
		self.printer("ssb : " + str(self.ssb))
		self.printer("TK / Kumo : " + str(self.TKKumo_percent(self.closetime)))
		self.printer("Price / Kumo :" + str(self.PriceKumo_percent(self.closetime, self.closePrice)))
		
		self.printer("")
		self.kumo_thickness_percent(self.closetime)
		self.tendancy()
		time.sleep(self.refreshRate)
		return json.dumps(self.res)

	def auto_update(self):
		if self.autoUpdate is True:
			self.klineHist = client.get_klines(symbol=self.symbol, interval=self.interval, limit=self.limit)


	def stop(self):
		self.is_running = False

	def get_high_low(self, period, initTime):
		current_time = initTime
		current_period = 1
		for i in self.klineHist:
			if float(i[6]) <= float(initTime):
				high = i[2]
				low = i[3]
				break
		for i in self.klineHist:
			if float(i[6]) <= float(initTime):
				if (high < i[2]):
					high = i[2]
				if (low > i[3]):
					low = i[3]
				if (float(i[6]) <= float(current_time) - self.timeScale):
					current_period += 1
					current_time = i[6]
					if (current_period >= period):
						break
		return (high, low)

	# average of the highest value and the lowest of the 9 last periods
	def tenkan_sen(self, initTime):
		(high, low) = self.get_high_low(self.TENKAN_P, initTime)
		return (float(high) + float(low)) / 2
	
	# average of the highest value and the lowest of the 26 last periods
	def kijun_sen(self, initTime):
		(high, low) = self.get_high_low(self.KIJUN_P, initTime)
		return (float(high) + float(low)) / 2

	# senkou span A (ssa) :  (Tenkan-sen + kijun-sen)/2 plotted 26 periods ahead.
	# senkou span B (ssb) : (highest high + lowest low)/2 calculated over the past 52 time periods and plotted 26 periods ahead. 
	def senkou_span(self, initTime):
		current_period = 1
		current_time = initTime
		for i in self.klineHist:
			if (i[6] <= current_time - self.timeScale):
				current_period += 1
			if current_period == self.SS_DISP:
				ssa = (float(self.tenkan_sen(i[6])) + float(self.kijun_sen(i[6]))) / 2
				(high, low) = self.get_high_low(self.SSB_P, i[6])
				ssb = (float(high) + float(low)) / 2
				return (ssa, ssb)

	def TK_percent(self, initTime):
		ts = self.tenkan_sen(initTime)
		ks = self.kijun_sen(initTime)
		return (ts - ks) / ks * 100

	def TKPrice_percent(self, initTime, closePrice):
		ts = self.tenkan_sen(initTime)
		ks = self.kijun_sen(initTime)
		Price = closePrice
		res = 0
		if Price > ts and Price > ks:
			if ts >= ks:
				res = (Price - ts) / ts * 100
			else:
				res = (Price - ks) / ks * 100
			return res + 1
		elif Price < ts and Price < ks:
			if ts <= ks:
				res = (Price - ts) / ts * 100
			else:
				res = (Price - ks) / ks * 100
			return res - 1
		else:
			return ((ts + ks) / 2 - closePrice) / closePrice 

	def TKKumo_percent(self, initTime):
		ts = self.tenkan_sen(initTime)
		ks = self.kijun_sen(initTime)
		ssa, ssb = self.senkou_span(initTime)
		TK_avg = (ts + ks) / 2
		kumo_avg = (ssa + ssb) / 2
		if TK_avg > ssa and TK_avg > ssb:
			if ssa >= ssb:
				res = (TK_avg - ssa) / ssa * 100
			else:
				res = (TK_avg - ssb) / ssb * 100
			return res + 1
		elif TK_avg < ssa and TK_avg < ssb:
			if ssa <= ssb:
				res = (TK_avg - ssa) / ssa * 100
			else:
				res = (TK_avg - ssb) / ssb * 100
			return res - 1
		else:
			return (TK_avg - kumo_avg) / kumo_avg

	def PriceKumo_percent(self, initTime, closePrice):

		ssa, ssb = self.senkou_span(initTime)
		Price = closePrice
		kumo_avg = (ssa + ssb) / 2
		if Price > ssa and Price > ssb:
			if ssa >= ssb:
				res = (Price - ssa) / ssa * 100
			else:
				res = (Price - ssb) / ssb * 100
			return res + 1
		elif Price < ssa and Price < ssb:
			if ssa <= ssb:
				res = (Price - ssa) / ssa * 100
			else:
				res = (Price - ssb) / ssb * 100
			return res - 1
		else:
			return (Price - kumo_avg) / kumo_avg 

	# Chikou span percentage calculation: today's closing price projected back 26 days on the chart.
	def chikou_span_percent(self, initTime):
		current_time = initTime
		for i in self.klineHist:
			if (i[6] <= current_time - (self.timeScale * self.CHIKOU_P)):
				return (float(self.closePrice) - float(i[4])) / float(i[4]) * 100
		self.printer("[chikou] : not enough data for chikou diff")

	# kumo is the cloud formed by the ssa and the ssb
	def kumo_thickness_percent(self, initTime):
		ssa, ssb = self.senkou_span(initTime)
		thickness = (ssa - ssb) / ssb * 100 
		self.printer('kumo thickness percentage ' + str(thickness))
		return thickness

	def tendancy(self):
		closeTime = self.closetime
		price_kumo = self.PriceKumo_percent(closeTime, self.closePrice)
		TK_perc = self.TK_percent(closeTime)
		TK_kumo = self.TKKumo_percent(closeTime)
		TK_price = self.TKPrice_percent(closeTime, self.closePrice)
		chikou_perc = self.chikou_span_percent(closeTime)
		self.res['CP'] = 0
		weight = 20
		self.printer("price = " + str(self.closePrice))
		self.printer("price difference = " + self.value_color(self.closePrice - self.last_price))
		if price_kumo > 1:
			self.res['PK'] = weight
		elif price_kumo < -1:
			self.res['PK'] = -weight
		self.printer("price / kumo : " + str(price_kumo) + " => " + self.value_color(self.res['PK']))
		if TK_perc > 0:
			self.res['TK'] = weight
		elif TK_perc < 0:
			self.res['TK'] = -weight
		self.printer("tenkan / kijun: " + str(TK_perc) + " => " + self.value_color(self.res['TK']))
		if TK_kumo > 1:
			self.res['TKK'] = weight
		elif TK_kumo < -1:
			self.res['TKK'] = -weight
		elif TK_kumo > 0:
			self.res['TKK'] = weight / 2
		else :
			self.res['TKK'] = -weight / 2
		self.printer("TK / kumo : " + str(TK_kumo) + " => " + self.value_color(self.res['TKK']))
		if chikou_perc > 0:
			self.res['CP'] = weight
		elif chikou_perc < 0:
			self.res['CP'] = -weight
		self.printer("chikou / price : " + str(chikou_perc) + " => " + self.value_color(self.res['CP']))
		if TK_price > 1:
			self.res['TKP'] = weight
		elif TK_price < -1:
			self.res['TKP'] = -weight
		elif TK_price > 0:
			self.res['TKP'] = weight / 2
		else :
			self.res['TKP'] = -weight / 2
		self.printer("TK / price : " + str(TK_price) + " => " + self.value_color(self.res['TKP']))
		res = self.res['PK'] + self.res['TK'] + self.res['TKK'] + self.res['CP'] + self.res['TKP']
		self.printer("confidence : " + self.value_color(res))
		self.res['tendancy'] = res
		self.last_price = self.closePrice

	def value_color(self, value):
		if value > 0:
			res = "\033[32m" + str(value) + "\033[0m"
		elif value < 0:
			res = "\033[91m" + str(value) + "\033[0m"
		else:
			res = str(value)
		return res

	def array_constructor(self, period):
		lines_data = {
			'tenkan' : [],
			'kijun' : [],
			'chikou' : [],
			'kumochikou' : [],
			'closePrice' : [],
			'ssa': [],
			'ssb' : [],
			'timestamps' : []
		}
		lines_data['tenkan'].append(self.ts)
		lines_data['kijun'].append(self.ks)
		lines_data['ssa'].append(self.ssa)
		lines_data['ssb'].append(self.ssb)
		lines_data['closePrice'].append(float(self.closePrice))
		lines_data['timestamps'].append(self.closetime / 1000)
		current_time = self.closetime
		current_period = 1
		lines_data['kumochikou'].append(float(self.closePrice))
		for i in self.klineHist:
			if (float(i[6]) <= float(current_time) - self.timeScale):
				if current_period < self.CHIKOU_P:
					lines_data['kumochikou'].append(float(i[4]))
				current_period += 1
				current_time = i[6]
				lines_data['timestamps'].append(i[6] / 1000)
				lines_data['tenkan'].append(self.tenkan_sen(current_time))
				lines_data['kijun'].append(self.kijun_sen(current_time))
				lines_data['ssa'].append(self.senkou_span(current_time)[0])
				lines_data['ssb'].append(self.senkou_span(current_time)[1])
				lines_data['closePrice'].append(float(i[4]))
			if (current_period >= period):
				break
		lines_data['kumochikou'] += lines_data['closePrice'][:-(self.CHIKOU_P)]
		lines_data['chikou'] = lines_data['closePrice'][:-(self.CHIKOU_P)]
		return lines_data

	def array_printer_launch(self, period=180):
		time.sleep(2)
		self.run()

		plt.ion()
		fig, axs = plt.subplots(2,2)
		fig.suptitle(self.symbol + ' in period of: ' + self.timescaleTitle)
		lines_data = self.array_constructor(period)
		lines = self.array_printer_init(lines_data, period, fig, axs)
		run = threading.Thread(target=self.array_printer_run, args=(fig, axs, lines, period, ))
		run.start()

	def array_printer_run(self, fig, axs, lines, period=180):
		self.is_running = True
		while self.is_running is True:
			self.run()
			lines_data = self.array_constructor(period)
			try :
				self.array_printer_update(lines_data, period, fig, axs, lines)
			except:
				traceback.print_exc()
				pass
			time.sleep(self.refreshRate)

	def array_printer_init(self, lines_data, period, fig, axs):
		lines = {
			'chikouPrice' : {
				'chikou' : None,
				'closePrice' : None,
				'kumochikouGreen' : None,
				'kumochikouRed' : None
			},
			'kumoPrice' : {
				'kumoRed' : None,
				'kumoGreen': None,
				'ssa': None,
				'ssb' : None,
				'closePrice' : None
			},
			'kumoTK' : {
				'kumoRed' : None,
				'kumoGreen': None,
				'ssa': None,
				'ssb' : None,
				'tenkan' : None,
				'kijun' : None
			},
			'allLines' : {
				'kumoRed' : None,
				'kumoGreen': None,
				'chikou' : None,
				'closePrice' : None,
				'ssa': None,
				'ssb' : None,
				'tenkan' : None,
				'kijun' : None
			}
		}

		palette = plt.get_cmap('Set1')
		chikouPrice = axs[0, 0]
		kumoPrice = axs[0, 1]
		kumoTK = axs[1, 0]
		allLines = axs[1, 1]

		tenkan = lines_data['tenkan'][::-1]
		kijun = lines_data['kijun'][::-1]
		kumochikou = lines_data['kumochikou'][::-1]
		chikou = lines_data['chikou'][::-1]
		ssa = lines_data['ssa'][::-1]
		ssb = lines_data['ssb'][::-1]
		closePrice = lines_data['closePrice'][::-1]

		x = np.arange(period)
		x_chikou = np.arange(period - self.CHIKOU_P)
		a_chikou = np.array(kumochikou)
		a_closePrice = np.array(closePrice)

		lines['chikouPrice']['chikou'] = chikouPrice.plot(x_chikou, chikou, color=palette(4), label='Chikou')
		chikouPrice.legend(loc="best")
		lines['chikouPrice']['closePrice'] = chikouPrice.plot(x, closePrice, color='black', label='Value')
		chikouPrice.legend(loc="best")
		chikouPrice.set_title("Chikou vs Price")
		lines['chikouPrice']['kumochikouGreen'] = chikouPrice.fill_between(x, kumochikou, closePrice, where=a_chikou >= a_closePrice, color='cyan', interpolate=True, label='Chikou>Price', alpha=0.2)
		chikouPrice.legend(loc="best")
		lines['chikouPrice']['kumochikouRed'] = chikouPrice.fill_between(x, kumochikou, closePrice, where=a_chikou <= a_closePrice, color='orange', interpolate=True, label='Chikou<Price', alpha=0.2)
		chikouPrice.legend(loc="best")

		a_ssa = np.array(ssa)
		a_ssb = np.array(ssb)

		lines['kumoPrice']['closePrice'] = kumoPrice.plot(x, closePrice, color='black', label='Value')
		kumoPrice.legend(loc="best")
		lines['kumoPrice']['ssa'] = kumoPrice.plot(x, ssa, color=palette(2), label='SSA')
		kumoPrice.legend(loc="best")
		lines['kumoPrice']['ssb'] = kumoPrice.plot(x, ssb, color=palette(3), label='SSB')
		kumoPrice.legend(loc="best")
		kumoPrice.set_title("Kumo vs Price")
		lines['kumoPrice']['kumoGreen'] = kumoPrice.fill_between(x, ssa, ssb, where=a_ssa >= a_ssb, color='green', interpolate=True, label='SSA>SSB', alpha=0.2)
		kumoPrice.legend(loc="best")
		lines['kumoPrice']['kumoRed'] = kumoPrice.fill_between(x, ssa, ssb, where=a_ssa <= a_ssb, color='red', interpolate=True, label='SSA<SSB', alpha=0.2)
		kumoPrice.legend(loc="best")

		lines['kumoTK']['kumoGreen'] = kumoTK.fill_between(x, ssa, ssb, where=a_ssa >= a_ssb, color='green', interpolate=True, label='SSA>SSB', alpha=0.2)
		kumoTK.legend(loc="best")
		lines['kumoTK']['kumoRed'] = kumoTK.fill_between(x, ssa, ssb, where=a_ssa <= a_ssb, color='red', interpolate=True, label='SSA<SSB', alpha=0.2)
		kumoTK.legend(loc="best")
		lines['kumoTK']['tenkan'] = kumoTK.plot(x, tenkan, color=palette(1), label='Tenkan')
		kumoTK.legend(loc="best")
		lines['kumoTK']['kijun'] = kumoTK.plot(x, kijun, color=palette(0), label='kijun')
		kumoTK.legend(loc="best")
		lines['kumoTK']['ssa'] = kumoTK.plot(x, ssa, color=palette(2), label='SSA')
		kumoTK.legend(loc="best")
		lines['kumoTK']['ssb'] = kumoTK.plot(x, ssb, color=palette(3), label='SSB')
		kumoTK.legend(loc="best")
		kumoTK.set_title("Kumo vs TK")

		lines['allLines']['kumoGreen'] = allLines.fill_between(x, ssa, ssb, where=a_ssa >= a_ssb, color='green', interpolate=True, label='SSA>SSB', alpha=0.2)
		allLines.legend(loc="best")
		lines['allLines']['kumoRed'] = allLines.fill_between(x, ssa, ssb, where=a_ssa <= a_ssb, color='red', interpolate=True, label='SSA<SSB', alpha=0.2)
		allLines.legend(loc="best")
		lines['allLines']['tenkan'] = allLines.plot(x, tenkan, color=palette(1), label='Tenkan')
		allLines.legend(loc="best")
		lines['allLines']['kijun'] = allLines.plot(x, kijun, color=palette(0), label='kijun')
		allLines.legend(loc="best")
		lines['allLines']['ssa'] = allLines.plot(x, ssa, color=palette(2), label='SSA')
		allLines.legend(loc="best")
		lines['allLines']['ssb'] = allLines.plot(x, ssb, color=palette(3), label='SSB')
		allLines.legend(loc="best")
		lines['allLines']['chikou'] = allLines.plot(x_chikou, chikou, color=palette(4), label='Chikou')
		allLines.legend(loc="best")
		lines['allLines']['closePrice'] = allLines.plot(x, closePrice, color='black', label='Value')
		allLines.legend(loc="best")
		allLines.set_title("All Lines")
		return lines

	def array_printer_update(self, lines_data, period, fig, axs, lines):

		# subplots
		chikouPrice = axs[0, 0]
		kumoPrice = axs[0, 1]
		kumoTK = axs[1, 0]
		allLines = axs[1, 1]

		# line list
		tenkan = lines_data['tenkan'][::-1]
		kijun = lines_data['kijun'][::-1]
		chikou = lines_data['chikou'][::-1]
		kumochikou = lines_data['kumochikou'][::-1]
		ssa = lines_data['ssa'][::-1]
		ssb = lines_data['ssb'][::-1]
		closePrice = lines_data['closePrice'][::-1]
	
		# creating arrays
		x = np.arange(period)
		x_chikou = np.arange(period - self.CHIKOU_P)
		a_ssa = np.array(ssa)
		a_ssb = np.array(ssb)

		# subplot chikou vs price
		a_chikou = np.array(kumochikou)
		a_closePrice = np.array(closePrice)

		lines['chikouPrice']['chikou'][0].remove()
		lines['chikouPrice']['chikou'] = chikouPrice.plot(x_chikou, chikou, color='orange', label='Chikou')
		lines['chikouPrice']['closePrice'][0].set_ydata(closePrice)
		if len(kumochikou) == len(x):
			lines['chikouPrice']['kumochikouGreen'].remove()
			lines['chikouPrice']['kumochikouGreen'] = chikouPrice.fill_between(x, kumochikou, closePrice, where=a_chikou >= a_closePrice, color='cyan', interpolate=True, label='Chikou<Price', alpha=0.2)
			lines['chikouPrice']['kumochikouRed'].remove()
			lines['chikouPrice']['kumochikouRed'] = chikouPrice.fill_between(x, kumochikou, closePrice, where=a_chikou <= a_closePrice, color='orange', interpolate=True, label='Chikou<Price', alpha=0.2)
	
		# subplot kumo vs price
		lines['kumoPrice']['closePrice'][0].set_ydata(closePrice)
		lines['kumoPrice']['ssa'][0].set_ydata(ssa)
		lines['kumoPrice']['ssb'][0].set_ydata(ssb)
		lines['kumoPrice']['kumoGreen'].remove()
		lines['kumoPrice']['kumoGreen'] = kumoPrice.fill_between(x, ssa, ssb, where=a_ssa >= a_ssb, color='green', interpolate=True, label='SSA>SSB', alpha=0.2)
		lines['kumoPrice']['kumoRed'].remove()
		lines['kumoPrice']['kumoRed'] = kumoPrice.fill_between(x, ssa, ssb, where=a_ssa <= a_ssb, color='red', interpolate=True, label='SSA<SSB', alpha=0.2)
		
		# subplot kumo vs tenkan and kijun
		lines['kumoTK']['kumoGreen'].remove()
		lines['kumoTK']['kumoGreen'] = kumoTK.fill_between(x, ssa, ssb, where=a_ssa >= a_ssb, color='green', interpolate=True, label='SSA>SSB', alpha=0.2)
		lines['kumoTK']['kumoRed'].remove()
		lines['kumoTK']['kumoRed'] = kumoTK.fill_between(x, ssa, ssb, where=a_ssa <= a_ssb, color='red', interpolate=True, label='SSA<SSB', alpha=0.2)
		lines['kumoTK']['tenkan'][0].set_ydata(tenkan)
		lines['kumoTK']['kijun'][0].set_ydata(kijun)
		lines['kumoTK']['ssa'][0].set_ydata(ssa)
		lines['kumoTK']['ssb'][0].set_ydata(ssb)

		# subplot of all lines
		lines['allLines']['kumoGreen'].remove()
		lines['allLines']['kumoGreen'] = allLines.fill_between(x, ssa, ssb, where=a_ssa >= a_ssb, color='green', interpolate=True, label='SSA>SSB', alpha=0.2)
		lines['allLines']['kumoRed'].remove()
		lines['allLines']['kumoRed'] = allLines.fill_between(x, ssa, ssb, where=a_ssa <= a_ssb, color='red', interpolate=True, label='SSA<SSB', alpha=0.2)
		lines['allLines']['tenkan'][0].set_ydata(tenkan)
		lines['allLines']['kijun'][0].set_ydata(kijun)
		lines['allLines']['ssa'][0].set_ydata(ssa)
		lines['allLines']['ssb'][0].set_ydata(ssb)
		lines['allLines']['closePrice'][0].set_ydata(closePrice)

		fig.canvas.draw()
		fig.canvas.flush_events()


if __name__ == "__main__":
	# print(ichimoku.time['15m'])
	# print(client.KLINE_INTERVAL_15MINUTE)
	indicator = ichimoku('15m', None, Client.KLINE_INTERVAL_15MINUTE, 'BTCUSDT', refreshRate=5, silent=False)
	indicator.array_printer_launch()
