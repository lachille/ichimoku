# Python Implementation of Ichimoku Stock Indicator

![Ichimoku stock indicator](ichimoku.png)

This project provides a Python implementation of the Ichimoku stock indicator, which is a popular technical analysis tool used in financial markets to identify trends and potential trading opportunities. The implementation utilizes the Binance API to retrieve stock data and generates a real-time graph to visualize the indicator's output.

## Prerequisites

Before running the program, ensure that you have the following prerequisites installed:

- Python 3: The programming language used for this project.
- `pip`: Python package installer, typically included with Python installations.
- `matplotlib`: A library used for creating graphs and visualizations.
- `python-binance`: A Python wrapper for the Binance API.
- `numpy`: A library used for numerical computations.

You can install the required packages by running the following command:

```
pip install matplotlib python-binance
```

## Getting Started

To get started with the project, follow these steps:

1. Clone the repository: `git clone https://github.com/your-username/ichimoku-indicator.git`.
2. Navigate to the project directory: `cd ichimoku-indicator`.
3. Export environment variables `BINANCE_KEY` and `BINANCE_SECRET_KEY`.
4. Run `pip install -r requirements.txt`.
    * If you don't have an API key, you can create one by signing up on the [Binance website](https://www.binance.com/). 
5. Run `python ichimoku.py`.

## Usage

Upon running the `ichimoku.py` file, the program will start fetching real-time stock data from Binance using the provided API credentials. It will then calculate the Ichimoku indicator values based on the retrieved data and display a live graph representing the indicator's output.

The program will continuously update the graph as new data arrives, allowing you to monitor the changes in the Ichimoku indicator in real-time. You can exit the program by pressing `Ctrl + C` in the terminal.

## Customization

The program can be customized according to your preferences. Here are a few possible modifications you can make:

- **Symbol**: By default, the program fetches data for the BTC/USDT trading pair. You can modify the `symbol` variable in the `ichimoku.py` file to fetch data for a different symbol.

- **Timeframe**: The program uses the 1-hour timeframe by default. You can change the `interval` variable in the `ichimoku.py` file to use a different timeframe. Supported values include '1m', '3m', '5m', '15m', '30m', '1h', '2h', '4h', '6h', '8h', '12h', '1d', '3d', '1w', '1M'.

- **Graph Customization**: You can modify the appearance and style of the graph by editing the relevant sections of the `ichimoku.py` file. The `matplotlib` library provides extensive customization options.

## Disclaimer

The Ichimoku indicator and any trading strategy based on it should be used with caution. This project and its developers do not provide financial advice or guarantee the accuracy or profitability of the indicator. Use the program at your own risk.
